# Project fitbit: a fitbit data analysis web based tool #

## Description
This project is a web tool to analyse the data generated from the fitbit smart watch. 
This application is able to run both remotely and locally for privacy.


## Features
Able to run locally.   
Line plots of: resting heart rate and regular heart rate.   
Bar plots of: calories, distance, altitude, step, and activity (combined bars).   

## dependencies
java 11.0.6  
gson 1.1.1
thymeleaf 4     
Tomcat 9.0.27   
javascript 1.8.5   
html 5  
css 3  
chartjs 2.9.3  
bootstrap 4.4.1  
jquery 3.4.1  

## Installation
#### from source
Clone the git repository.  
`git clone https://<yourUserName@bitbucket.org/tom_wagenaar/projectfitbit.git`

Open project in intellij from where the repo is cloned.  
Click the add configuration button and select tomcat local under templates.  
Then click configure at application server and browse to your local tomcat folder.   
Then go to deployment and click on the + icon at deploy at start up and select artifact.   
Replace the application context with just: /  
Click on apply and the ok.  
Press the run button and that should run the web server.  

#### compiled 
TODO

#### settings at web.xml  
You can configure the output folder for the uploaded files at the web.xml at src/main/webapp/web.xml.  
Change SystemTemp to your folder path at: `<param-value>SystemTemp</param-value>`  