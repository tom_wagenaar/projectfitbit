package FitbitWebtool.Service;

import FitbitWebtool.Model.HeartBeatStats;
import FitbitWebtool.Model.SimpleJsonStats;

import java.util.ArrayList;

/**
 * object used by gson to create a json with the data object in it
 */
public class FormattedOutput {
    private HeartBeatStats.FormattedHeartBeats dataHeartRate = new HeartBeatStats.FormattedHeartBeats(new ArrayList<>(),new ArrayList<>());
    private HeartBeatStats.FormattedHeartBeats dataRHeartRate = new HeartBeatStats.FormattedHeartBeats(new ArrayList<>(),new ArrayList<>());
    private SimpleJsonStats.FormattedValues dataCalories = new SimpleJsonStats.FormattedValues(new ArrayList<>(),new ArrayList<>());
    private SimpleJsonStats.FormattedValues dataDistance = new SimpleJsonStats.FormattedValues(new ArrayList<>(),new ArrayList<>());
    private SimpleJsonStats.FormattedValues dataAltitude = new SimpleJsonStats.FormattedValues(new ArrayList<>(),new ArrayList<>());
    private SimpleJsonStats.FormattedValues dataSteps = new SimpleJsonStats.FormattedValues(new ArrayList<>(),new ArrayList<>());

    private SimpleJsonStats.FormattedValues dataSedentaryMins = new SimpleJsonStats.FormattedValues(new ArrayList<>(),new ArrayList<>());
    private SimpleJsonStats.FormattedValues dataLightlyMins = new SimpleJsonStats.FormattedValues(new ArrayList<>(),new ArrayList<>());
    private SimpleJsonStats.FormattedValues dataModerateMins = new SimpleJsonStats.FormattedValues(new ArrayList<>(),new ArrayList<>());
    private SimpleJsonStats.FormattedValues dataVeryActiveMins = new SimpleJsonStats.FormattedValues(new ArrayList<>(),new ArrayList<>());


    //TODO put Simple json stats into a list
    public FormattedOutput(HeartBeatStats.FormattedHeartBeats dataHeartRate,
                           HeartBeatStats.FormattedHeartBeats dataRHeartRate,
                           SimpleJsonStats.FormattedValues dataCalories,
                           SimpleJsonStats.FormattedValues dataDistance,
                           SimpleJsonStats.FormattedValues dataAltitude,
                           SimpleJsonStats.FormattedValues dataSteps,
                           SimpleJsonStats.FormattedValues dataSedentaryMins,
                           SimpleJsonStats.FormattedValues dataLightlyMins,
                           SimpleJsonStats.FormattedValues dataModerateMins,
                           SimpleJsonStats.FormattedValues dataVeryActiveMins) {
        this.setDataHeartRate(dataHeartRate);
        this.setDataRHeartRate(dataRHeartRate);
        this.setDataCalories(dataCalories);
        this.setDataDistance(dataDistance);
        this.setDataAltitude(dataAltitude);
        this.setDataSteps(dataSteps);

        this.setDataSedentaryMins(dataSedentaryMins);
        this.setDataLightlyMins(dataLightlyMins);
        this.setDataModerateMins(dataModerateMins);
        this.setDataVeryActiveMins(dataVeryActiveMins);


    }

    public HeartBeatStats.FormattedHeartBeats getDataHeartRate() {
        return dataHeartRate;
    }

    public void setDataHeartRate(HeartBeatStats.FormattedHeartBeats dataHeartRate) {
        this.dataHeartRate = dataHeartRate;
    }

    public HeartBeatStats.FormattedHeartBeats getDataRHeartRate() {
        return dataRHeartRate;
    }

    public void setDataRHeartRate(HeartBeatStats.FormattedHeartBeats dataRHeartRate) {
        this.dataRHeartRate = dataRHeartRate;
    }

    public SimpleJsonStats.FormattedValues getDataCalories() {
        return dataCalories;
    }

    public void setDataCalories(SimpleJsonStats.FormattedValues dataCalories) {
        this.dataCalories = dataCalories;
    }

    public SimpleJsonStats.FormattedValues getDataDistance() {
        return dataDistance;
    }

    public void setDataDistance(SimpleJsonStats.FormattedValues dataDistance) {
        this.dataDistance = dataDistance;
    }

    public SimpleJsonStats.FormattedValues getDataAltitude() {
        return dataAltitude;
    }

    public void setDataAltitude(SimpleJsonStats.FormattedValues dataAltitude) {
        this.dataAltitude = dataAltitude;
    }

    public SimpleJsonStats.FormattedValues getDataSteps() {
        return dataSteps;
    }

    public void setDataSteps(SimpleJsonStats.FormattedValues dataSteps) {
        this.dataSteps = dataSteps;
    }

    public SimpleJsonStats.FormattedValues getDataSedentaryMins() {
        return dataSedentaryMins;
    }

    public void setDataSedentaryMins(SimpleJsonStats.FormattedValues dataSedentaryMins) {
        this.dataSedentaryMins = dataSedentaryMins;
    }

    public SimpleJsonStats.FormattedValues getDataLightlyMins() {
        return dataLightlyMins;
    }

    public void setDataLightlyMins(SimpleJsonStats.FormattedValues dataLightlyMins) {
        this.dataLightlyMins = dataLightlyMins;
    }

    public SimpleJsonStats.FormattedValues getDataModerateMins() {
        return dataModerateMins;
    }

    public void setDataModerateMins(SimpleJsonStats.FormattedValues dataModerateMins) {
        this.dataModerateMins = dataModerateMins;
    }

    public SimpleJsonStats.FormattedValues getDataVeryActiveMins() {
        return dataVeryActiveMins;
    }

    public void setDataVeryActiveMins(SimpleJsonStats.FormattedValues dataVeryActiveMins) {
        this.dataVeryActiveMins = dataVeryActiveMins;
    }
}
