package FitbitWebtool.Service;

/**
 * object used by gjson to parse the json
 */
public class RestingHeartRate {
    private String dateTime;
    private Value value;

    public Value getValue() {
        return value;
    }

    public void setValue(Value value) {
        this.value = value;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }


    public static class Value {
        private float value;
        private float error;
        private String date;

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public float getError() {
            return error;
        }

        public void setError(float error) {
            this.error = error;
        }

        public float getValue() {
            return value;
        }

        public void setValue(float value) {
            this.value = value;
        }
    }
}
