package FitbitWebtool.Service;

/**
 * object used by gjson to parse the json
 */
public class HeartBeat {
    // Instance variable declaration.
    private String dateTime;
    private Value value;

    public HeartBeat(String dateTime, int bpm) {
        this.setDateTime(dateTime);
        this.setBpm(bpm);
    }
    public void setBpm(int bpm) {
        this.value = new Value(bpm,-1);
    }


    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public Value getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "HeartBeat{" +
                "bpm=" + value.getBpm() +
                "dateTime=" + dateTime +
                ", confidence=" +"Not implemented" +
                '}';
    }

    public static class Value {
        private int bpm;
        private int confidence;

        public Value(int bpm,int confidence) {
            this.bpm = bpm;
            this.confidence = confidence;
        }

        public int getBpm() {
            return bpm;
        }

        public void setBpm(int bpm) {
            this.bpm = bpm;
        }

        public int getConfidence() {
            return confidence;
        }

        public void setConfidence(int confidence) {
            this.confidence = confidence;
        }
    }
}
