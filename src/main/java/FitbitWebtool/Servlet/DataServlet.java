package FitbitWebtool.Servlet;


import FitbitWebtool.Service.FormattedOutput;
import FitbitWebtool.Model.ParseJson;
import FitbitWebtool.Model.ParseZipFile;
import com.google.gson.Gson;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "DataServlet", urlPatterns = "/data")
public class DataServlet extends HttpServlet {

    /**
     * Retrieves data regarding recent earthquakes from the KNMI. Data is returned as JSON.
     * @param request
     * @param response
     * @throws IOException
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        // TODO: extra parameter to get different data sources

        //request.getServletContext().getInitParameter("current-filepath");

        String completeFilePath = getServletContext().getAttribute("filePathLast").toString();
        ParseZipFile.parseZip(completeFilePath);
        String adjustedFilePath = completeFilePath.replace(".zip", "");


        // Get JSON data as String
        FormattedOutput formattedOutput = ParseJson.unpackFolder(adjustedFilePath);
        String outputJson = new Gson().toJson(formattedOutput);

        formattedOutput.getDataModerateMins();

        // Return data as JSON
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(outputJson);
    }
}