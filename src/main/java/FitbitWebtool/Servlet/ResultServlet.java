package FitbitWebtool.Servlet;

import FitbitWebtool.WebConfig;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

//import org.apache.commons.math3.stat.inference.TTest;

@WebServlet(name = "ResultServlet", urlPatterns = "/resultPage", loadOnStartup = 2)
public class ResultServlet extends HttpServlet {
    // Instance variable declaration.
    private TemplateEngine templateEngine;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config); //required in this implementation
        System.out.println("[ResultServlet] Running init(ServletConfig config)");
        this.templateEngine = WebConfig.getTemplateEngine();
    }

    @Override
    public void destroy() {
        super.destroy();
        System.out.println("[ResultServlet] Shutting down servlet service");
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        WebContext ctx = new WebContext(request,
                response,
                request.getServletContext(),
                request.getLocale());

        templateEngine.process("resultPage", ctx, response.getWriter());
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException{
        WebContext ctx = new WebContext(request, response, request.getServletContext(), request.getLocale());

        templateEngine.process("resultPage", ctx, response.getWriter());

    }

}
