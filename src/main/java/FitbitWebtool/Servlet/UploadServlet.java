package FitbitWebtool.Servlet;

import FitbitWebtool.Model.ParseZipFile;
import FitbitWebtool.Model.UploadedFile;
import FitbitWebtool.WebConfig;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "UploadServlet", urlPatterns = "/upload", loadOnStartup = 1)
public class UploadServlet extends HttpServlet {
    // Instance variable declaration.
    private TemplateEngine templateEngine;
    private static final long serialVersionUID = 1L;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config); //required in this implementation
        System.out.println("[UploadServlet] Running init(ServletConfig config)");
        this.templateEngine = WebConfig.getTemplateEngine();
    }

    @Override
    public void destroy() {
        super.destroy();
        System.out.println("[UploadServlet] Shutting down servlet service");
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        //TODO double check variable names
        //TODO only upload 1 file
        try {
            //TODO catch maximum file size error
            UploadedFile fitbitZipFile = new UploadedFile(
                    50 * 1024 * 100,
                    4 * 1024,
                    request,
                    response,getServletContext()
            );

            //create the upload folder and save the file
            fitbitZipFile.createUploadFolder();
            fitbitZipFile.saveUploadToFile();

            //useful getters
            String fileName = fitbitZipFile.getFileName();

            //The complete path of uploaded file on the server
            String completeFilePath = fitbitZipFile.getFilePath();

            WebContext ctx = new WebContext(request, response, request.getServletContext(), request.getLocale());

            // Check if the file is a zip file.
            if (completeFilePath.endsWith(".zip")) {
                // Get the complete pathway of the zip file and give it to the ParseZipFile class.
                ParseZipFile.parseZip(completeFilePath);

                // Remove the .zip from the pathway string.
                String adjustedFilePath = completeFilePath.replace(".zip", "");

                // Get a Servlet object.
                ServletContext application = getServletConfig().getServletContext();

                // Set a attribute that has the uploadfilepath + a specific folder that is the same for every user.
                application.setAttribute("filePathLast", adjustedFilePath + "/user-site-export/");

                ctx.setVariable("message", "uploaded "+ fileName +" successfully!");
                templateEngine.process("resultPage", ctx, response.getWriter());

            } else {
                ctx.setVariable("message", "Please provide a zip file, not " + fileName + ". Please go back to the home page.");
                templateEngine.process("upload", ctx, response.getWriter());
            }
            }

        catch(Exception ex) {
            System.out.println(ex);
        }
    }
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException{
        //simply go back to upload form
        WebContext ctx = new WebContext(request,
                                        response,
                                        request.getServletContext(),
                                        request.getLocale());

        templateEngine.process("upload", ctx, response.getWriter());
    }
}