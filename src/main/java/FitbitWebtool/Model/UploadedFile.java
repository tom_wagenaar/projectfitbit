package FitbitWebtool.Model;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.lang.RandomStringUtils;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.util.Iterator;
import java.util.List;

public class UploadedFile {
    private final HttpServletRequest request;
    private final HttpServletResponse response;
    private int maxFileSize;
    private int maxMemSize;
    private String tmpFolder;
    private String UploadFolderPath = null;
    File file ;
    private String FileName = null;
    private String FilePath = null;
    private String UniqueFileName = null;

    public UploadedFile(int maxFileSize,int maxMemSize,HttpServletRequest request,
                        HttpServletResponse response,ServletContext servletContext) throws Exception {
        this.maxFileSize = maxFileSize;
        this.maxMemSize = maxMemSize;
        this.request =  request;
        this.response = response;
        this.tmpFolder = getTmpFolder(servletContext);
    }


    /**
     * Gets the temp folder from where the server is running and returns it and saves it in this.tmpFolder
     * @param servletContext
     * @return tempFolder path as String
     */
    public static String getTmpFolder(ServletContext servletContext) {
        String tmpFolder;
        String TmpFolderXml = servletContext.getInitParameter("temp-folder");

        if (TmpFolderXml.equals("SystemTemp")) {
            tmpFolder = System.getProperty("java.io.tmpdir");
        } else {
            tmpFolder = TmpFolderXml;
        }
        return tmpFolder;
    }


    /**
     * Creates the upload folder with this.tmpFolder
     */
    public void createUploadFolder() {
        try {
            this.UploadFolderPath = this.tmpFolder + File.separator + "uploads";

            File UploadFolder = new File(this.UploadFolderPath);

            UploadFolder.setWritable(true);
            UploadFolder.setReadable(true);
            UploadFolder.setExecutable(true);

            if (!UploadFolder.exists()) {
                System.out.println("folder doesn't exist. creating folder: " + this.UploadFolderPath);
            } else {
                boolean bool = UploadFolder.mkdir();
                if (bool) {
                    System.out.println("Directory created successfully");
                } else {
                    System.out.println("Sorry couldn’t create specified directory");
                }
            }
        } catch (Exception ex) {
            System.out.println(ex);
        }
    }

    /**
     * saves the uploaded file to disk and saves this.FileName, this.UniqueFileName, this.FilePath
     * And prints if the upload is succesfull
     * @throws Exception
     */
    public void saveUploadToFile() throws Exception {

        //Set upload file type
        response.setContentType("text/html");

        DiskFileItemFactory factory = new DiskFileItemFactory();

        // maximum size that will be stored in memory
        factory.setSizeThreshold(this.maxMemSize);

        // Location to save data that is larger than maxMemSize.
        factory.setRepository(new File(this.tmpFolder));

        // Create a new file upload handler
        ServletFileUpload upload = new ServletFileUpload(factory);

        // maximum file size to be uploaded.
        upload.setSizeMax( maxFileSize );

        // Parse the request to get file items.
        List fileItems = upload.parseRequest(request);

        // Process the uploaded file items
        Iterator filesIter = fileItems.iterator();
        FileItem fi = (FileItem)filesIter.next();

        //todo DETECT IF MULTPLE FILES?

        // save original filename
        this.FileName = fi.getName();
        //TODO use string formating

        //create a unique file name to prevent replacing old files
        this.UniqueFileName =  RandomStringUtils.randomAlphanumeric(15) + "_" + FileName;
        this.FilePath = this.UploadFolderPath + File.separator + this.UniqueFileName;
        file = new File( this.FilePath) ;
        fi.write( file ) ;
        System.out.println("uploaded " + FileName + " to destination: " + this.FilePath);
    }

    /**
     * @return
     */
    public String getFilePath() {
        return this.FilePath;
    }

    /**
     * @return folder path to the upload folder on the server
     */
    public String getUploadFolderPath() {
        return this.UploadFolderPath;
    }

    /**
     * @return FileName of the file uploaded by the user
     */
    public String getFileName() {
        return this.FileName;
    }

    /**
     * @return FileName of the file on disk of the server
     */
    public String getUniqueFileName() { return this.UniqueFileName;}

}
