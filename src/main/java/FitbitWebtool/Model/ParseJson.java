package FitbitWebtool.Model;

// Import declaration.

import FitbitWebtool.Service.FormattedOutput;
import FitbitWebtool.Service.HeartBeat;
import FitbitWebtool.Service.RestingHeartRate;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Tom Wagenaar
 * @version 0.0.2
 * Date: 16-12-2019
 */
public class ParseJson {
    // Instance variable declaration.
    //TODO try catch if file contains wrong format

    /**
     * @param uploadFolderPath
     * @return
     * @throws IOException
     */
    // Methods
    //TODO refactor to make more consistent naming
    public static FormattedOutput unpackFolder(String uploadFolderPath) throws IOException {

        // Make a variable that holds the uploadFolder as a file.
        final File uploadFolder = new File(uploadFolderPath);
        System.out.println(uploadFolder.toString());

        // TODO: When the user provides a zip file it is possible that there won't be a user-site-export folder in it
        //  this is due to the fact that the user didn't requested the data using the fitbit whole data request.

        List<HeartBeat> dataHearRate = new ArrayList<>();
        List<RestingHeartRate> dataRHeartRate = new ArrayList<>();
        //TODO put all simple jsons in one object
        List<SimpleJsonStats.Value> dataCalories = new ArrayList<>();
        List<SimpleJsonStats.Value>  dataDistance = new ArrayList<>();
        List<SimpleJsonStats.Value> dataAltitude = new ArrayList<>();
        List<SimpleJsonStats.Value> dataSteps = new ArrayList<>();
        List<SimpleJsonStats.Value> dataSedentaryMins = new ArrayList<>();
        List<SimpleJsonStats.Value> dataLightlyActiveMins = new ArrayList<>();
        List<SimpleJsonStats.Value> dataModerateActiveMins = new ArrayList<>();
        List<SimpleJsonStats.Value> dataVeryActiveMins = new ArrayList<>();



        // Create a type to convert to
        Type listTypeHeartBeat = new TypeToken<List<HeartBeat>>() {
        }.getType();
        Type listTypeRestingHeartRate = new TypeToken<List<RestingHeartRate>>() {
        }.getType();
        Type listTypeValues = new TypeToken<List<SimpleJsonStats.Value>>() {
        }.getType();

        for (final File fileEntry : uploadFolder.listFiles()) {
            // Change the fileEntry to a string.
            String fileName = fileEntry.getName();
            try (FileReader reader = new FileReader(fileEntry)) {
                // For every fileName that starts with heart-rate, give it to another method.
                //TODO switch
                if (fileName.startsWith("heart_rate")) {
                    dataHearRate.addAll(new Gson().fromJson(reader, listTypeHeartBeat));
                } else if (fileName.startsWith("resting_heart_rate")) {
                    dataRHeartRate.addAll(new Gson().fromJson(reader, listTypeRestingHeartRate));
                } else if (fileName.startsWith("calories")) {
                    dataCalories.addAll(new Gson().fromJson(reader, listTypeValues));
                } else if (fileName.startsWith("distance")) {
                    dataDistance.addAll(new Gson().fromJson(reader, listTypeValues));
                } else if (fileName.startsWith("altitude")) {
                    dataAltitude.addAll(new Gson().fromJson(reader, listTypeValues));
                } else if (fileName.startsWith("steps")) {
                    dataSteps.addAll(new Gson().fromJson(reader, listTypeValues));
                } else if (fileName.startsWith("sedentary_minutes")) {
                    dataSedentaryMins.addAll(new Gson().fromJson(reader, listTypeValues));
                } else if (fileName.startsWith("lightly_active_minutes")) {
                    dataLightlyActiveMins.addAll(new Gson().fromJson(reader, listTypeValues));
                } else if (fileName.startsWith("moderately_active_minutes")) {
                    dataModerateActiveMins.addAll(new Gson().fromJson(reader, listTypeValues));
                } else if (fileName.startsWith("very_active_minutes")) {
                    dataVeryActiveMins.addAll(new Gson().fromJson(reader, listTypeValues));
                }
            }

        }
        //custom json files
        HeartBeatStats heartBeatStats = new HeartBeatStats();
        heartBeatStats.setHeartBeats(dataHearRate);
        HeartBeatStats restingHeartBeatStats = new HeartBeatStats();
        restingHeartBeatStats.setFromRestingHeartRate(dataRHeartRate);

        //simple json files
        //TODO check if the intervals are correct
        //exercise
        SimpleJsonStats caloriesStats = new SimpleJsonStats(60);
        caloriesStats.setUnFormattedValues(dataCalories);
        SimpleJsonStats distanceStats = new SimpleJsonStats(60);
        distanceStats.setUnFormattedValues(dataDistance);
        SimpleJsonStats altitudeStats = new SimpleJsonStats(60);
        altitudeStats.setUnFormattedValues(dataAltitude);
        SimpleJsonStats stepsStats = new SimpleJsonStats(60);
        stepsStats.setUnFormattedValues(dataSteps);

        //activity
        SimpleJsonStats sedentaryMinsStats = new SimpleJsonStats(86400);
        sedentaryMinsStats.setUnFormattedValues(dataSedentaryMins);
        SimpleJsonStats lightlyActiveMinsStats = new SimpleJsonStats(86400);
        lightlyActiveMinsStats.setUnFormattedValues(dataLightlyActiveMins);
        SimpleJsonStats moderateActiveMinsStats = new SimpleJsonStats(86400);
        moderateActiveMinsStats.setUnFormattedValues(dataModerateActiveMins);
        SimpleJsonStats veryActiveMinsStats = new SimpleJsonStats(86400);
        veryActiveMinsStats.setUnFormattedValues(dataVeryActiveMins);

        FormattedOutput formattedOutput = new FormattedOutput(
                heartBeatStats.getFHAverages(300),
                restingHeartBeatStats.getFormattedHeartBeats(86400), //set to one day
                caloriesStats.getAverages(40),
                distanceStats.getAverages(100),
                altitudeStats.getAverages(300),
                stepsStats.getAverages(180),
                sedentaryMinsStats.getAverages(0),
                lightlyActiveMinsStats.getAverages(0),
                moderateActiveMinsStats.getAverages(0),
                veryActiveMinsStats.getAverages(0)
        );
        return formattedOutput;
    }
}
