package FitbitWebtool.Model;

import FitbitWebtool.Service.HeartBeat;
import FitbitWebtool.Service.RestingHeartRate;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Object that takes List<HeartBeat> and creates a formatted version of that list that has its gaps in time filled with
 * 0 values. And has functions to create statistics of those bpms.
 */
public class HeartBeatStats {
    private List<HeartBeat> heartBeats = new ArrayList<>();
    private List<RestingHeartRate> RestingHeartBeats = new ArrayList<>();
    private FormattedHeartBeats formattedHeartBeats = new FormattedHeartBeats(new ArrayList<>(),new ArrayList<>());

    /**
     * set the unformatted heartBeat list
     * @param HeartBeats List<HeartBeat>
     */
    public void setHeartBeats(List<HeartBeat> HeartBeats) {
        this.heartBeats = HeartBeats;
    }

    /**
     * @return return heartBeats
     */
    public List<HeartBeat> getHeartBeats() {
        return this.heartBeats;
    }

    //TODO make more efficient by formattind directly since we loop through this data twice to format it again
    public List<HeartBeat> setFromRestingHeartRate(List<RestingHeartRate> restingHeartRates) {
        List<HeartBeat> newHeartBeats = new ArrayList<>();
        for (RestingHeartRate restingHeartRate: restingHeartRates) {
            newHeartBeats.add(new HeartBeat(restingHeartRate.getDateTime(),(int) restingHeartRate.getValue().getValue()));
        }
        this.heartBeats = newHeartBeats;
        return newHeartBeats;
    }

    /**
     * Fills in gaps in heartBeats with 0  values
     * @return FormattedHeartBeats
     */
    private FormattedHeartBeats createFormattedHeartBeats(int intervalSec) {
        this.formattedHeartBeats.values.clear();
        this.formattedHeartBeats.dateTimes.clear();
        SimpleDateFormat sdfInput = new SimpleDateFormat("MM/dd/yy HH:mm:ss");
        SimpleDateFormat sdfoutput = new SimpleDateFormat("MM/dd");
        Date lastDateTime = new Date();
        Date currentDateTime;
        for (HeartBeat heartBeat : this.heartBeats) {
            try {
                currentDateTime = sdfInput.parse(heartBeat.getDateTime());

                //prevent lastDateTime to be 0 on the first iteration
                if (lastDateTime.getTime() == 0) {
                    lastDateTime = currentDateTime;
                }

                //find gaps and fill them in
                long currentTime = currentDateTime.getTime();
                long timeDiff = (currentDateTime.getTime() - lastDateTime.getTime()) / (intervalSec*1000L);
                if (timeDiff > 1) {
                    for (int i = 0; i <= timeDiff; i++) {
                        //add empty value and time
                        currentDateTime.setTime(currentTime + 5000L * i);
                        this.formattedHeartBeats.values.add(0);
                        this.formattedHeartBeats.dateTimes.add(sdfoutput.format(currentDateTime));
                    }

                } else {
                    //set normal values
                    this.formattedHeartBeats.values.add(heartBeat.getValue().getBpm());
                    this.formattedHeartBeats.dateTimes.add(sdfoutput.format(currentDateTime));
                }
                lastDateTime = currentDateTime;
            } catch (ParseException e) {
                e.printStackTrace();
            }


        }
        return this.formattedHeartBeats;
    }

    /**
     * returns FormattedHeartBeats
     * @return FormattedHeartBeats
     */
    public FormattedHeartBeats getFormattedHeartBeats(int intervalSec) {
        //create the FormattedHearBeats if we dont have them already
        if (this.formattedHeartBeats.values.isEmpty()) {
            this.createFormattedHeartBeats(intervalSec);
        }
        return this.formattedHeartBeats;
    }

    /**
     * @param AvgSize size of the averages each 5 second interval
     * @return FormattedHeartBeats with averages
     */
    public FormattedHeartBeats getFHAverages(int AvgSize) {
        this.getFormattedHeartBeats(5);
        FormattedHeartBeats FHBeatsWithAverages = new FormattedHeartBeats(new ArrayList<Integer>(),new ArrayList<String>());
        List<Integer> currentBpms = new ArrayList<Integer>();
        for (int i = 0; i < this.formattedHeartBeats.values.size(); i++) {
            currentBpms.add(this.formattedHeartBeats.getValues().get(i));
            //create a list to use to calc the averages
            if (currentBpms.size() >= AvgSize) {
                String dateTime = this.formattedHeartBeats.dateTimes.get(i - AvgSize / 2);
                //calc average and add it to the new list
                int avgBpm = calcAvg(currentBpms);
                FHBeatsWithAverages.values.add(avgBpm);
                FHBeatsWithAverages.dateTimes.add(dateTime);
                currentBpms.clear();
            }
        }
        return FHBeatsWithAverages;
    }

    /**
     * Calculates the average from a list of bpms and excludes 0 values
     * @param bpms list of bpms to calculate the average from
     * @return the average
     */
    private static int calcAvg(List<Integer> bpms) {
        int total = 0;
        int zeroFound = 0;
        for (int bpm:bpms) {
            if (bpm == 0) {
                zeroFound++;
                if (zeroFound >= bpms.size()) {
                    return 0;
                }
            } else  {
                total += bpm;
            }
        }
        return total / (bpms.size() - zeroFound);
    }

    public List<RestingHeartRate> getRestingHeartBeats() {
        return RestingHeartBeats;
    }

    public void setRestingHeartBeats(List<RestingHeartRate> restingHeartBeats) {
        RestingHeartBeats = restingHeartBeats;
    }


    /**
     * A object used in creating the output json with gson.
     * It contains a bpms list and dateTimes list.
     */
    public static class FormattedHeartBeats {

        //TODO make sure the 2 list are always the same size
        public FormattedHeartBeats(ArrayList<Integer> values, ArrayList<String>  dateTimes) {
            this.setValues(values);
            this.setDateTimes(dateTimes);
        }
        private ArrayList<Integer> values;
        private ArrayList<String> dateTimes;

        public ArrayList<Integer> getValues() {
            return values;
        }

        public void setValues(ArrayList<Integer> values) {
            this.values = values;
        }

        public  ArrayList<String> getDateTimes() {
            return dateTimes;
        }

        public void setDateTimes(ArrayList<String> dateTimes) {
            this.dateTimes = dateTimes;
        }

    }

    public static void main(String[] args) {
    }



}

