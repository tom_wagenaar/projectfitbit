package FitbitWebtool.Model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Formatting and statistics for json with dateTime and value as float
 */
public class SimpleJsonStats {
    private List<Value> unFormattedValues;
    private FormattedValues formattedValues = new FormattedValues(new ArrayList<>(), new ArrayList<>());
    private int interval;

    public List<Value> getUnFormattedValues() {
        return unFormattedValues;
    }

    public SimpleJsonStats(int interval) {
        setInterval(interval);
    }

    /**
     * set the unFormattedValues as a list of Value objects
     * @param unFormattedValues list of Value objects
     */
    public void setUnFormattedValues(List<Value> unFormattedValues) {
        this.unFormattedValues = unFormattedValues;
    }

    /**
     * returns the formatted values and creates them if they don't exist yet
     * @return formattedValues
     */
    public FormattedValues getFormattedValues() {
        if (this.formattedValues.values.isEmpty()) {
            this.createFormattedValues(this.interval);
        }
        return formattedValues;
    }

    /**
     * createFormattedValues by removing gaps
     * @param intervalSec
     */
    public void createFormattedValues(int intervalSec) {
        //TODO generalise this function
        this.formattedValues.values.clear();
        this.formattedValues.dateTimes.clear();
        SimpleDateFormat sdfInput = new SimpleDateFormat("MM/dd/yy HH:mm:ss");
        SimpleDateFormat sdfoutput = new SimpleDateFormat("MM/dd");
        Date lastDateTime = new Date();
        Date currentDateTime;
        System.out.println("formatting values: "+this.unFormattedValues.size());
        for (Value value : this.unFormattedValues) {
            try {
                currentDateTime = sdfInput.parse(value.getDateTime());

                //prevent lastDateTime to be 0 on the first iteration
                if (lastDateTime.getTime() == 0) {
                    lastDateTime = currentDateTime;
                }

                //find gaps and fill them in
                long currentTime = currentDateTime.getTime();
                long timeDiff = (currentDateTime.getTime() - lastDateTime.getTime()) / (intervalSec*1000L);
                if (timeDiff > 1) {
                    for (int i = 0; i <= timeDiff; i++) {
                        //add empty value and time
                        currentDateTime.setTime(currentTime + 5000L * i);
                        this.formattedValues.values.add(0F);
                        this.formattedValues.dateTimes.add(sdfoutput.format(currentDateTime));
                    }

                } else {
                    //set normal values
                    this.formattedValues.values.add(value.getValue());
                    this.formattedValues.dateTimes.add(sdfoutput.format(currentDateTime));
                }
                lastDateTime = currentDateTime;
            } catch (ParseException e) {
                e.printStackTrace();
            }

        }

    }

    /**
     * @param AvgSize size of the averages each 5 second interval
     * @return FormattedHeartBeats with averages
     */
    public FormattedValues getAverages(int AvgSize) {
        this.getFormattedValues();
        FormattedValues valueAvg = new FormattedValues(new ArrayList<>(),new ArrayList<>());
        List<Float> currentValues = new ArrayList<>();
        for (int i = 0; i < this.formattedValues.values.size(); i++) {
            currentValues.add(this.formattedValues.getValues().get(i));
            //create a list to use to calc the averages
            if (currentValues.size() >= AvgSize) {
                String dateTime = this.formattedValues.dateTimes.get(i - AvgSize / 2);
                //calc average and add it to the new list
                float avgBpm = calcAvg(currentValues);
                valueAvg.values.add(avgBpm);
                valueAvg.dateTimes.add(dateTime);
                currentValues.clear();
            }
        }
        return valueAvg;
    }

    /**
     * calculates the average from a list of floats while ignoring 0 values
     * @param values list of floats
     * @return average as float
     */
    private float calcAvg(List<Float> values) {
        float total = 0;
        float zeroFound = 0;
        for (float Value:values) {
            if (Value == 0) {
                zeroFound++;
                if (zeroFound >= values.size()) {
                    return 0;
                }
            } else  {
                total += Value;
            }
        }
        return total / (values.size() - zeroFound);
    }

    /**
     * gets the interval used to fill gaps
     * @return interval as int
     */
    public int getInterval() {
        return interval;
    }

    /**
     * returns the interval used to fill gaps
     * @param interval as int
     */
    public void setInterval(int interval) {
        this.interval = interval;
    }

    /**
     * Object used to parse the json using gson
     */
    public class Value {
        private String dateTime;
        private float value;
        private FormattedValues formattedValue;

        public String getDateTime() {
            return dateTime;
        }

        public void setDateTime(String dateTime) {
            this.dateTime = dateTime;
        }

        public float getValue() {
            return value;
        }

        public void setValue(float value) {
            this.value = value;
        }
    }

    /**
     * object used to create the formatted values
     */
    public static class FormattedValues {
        public FormattedValues(List<String> dateTimes, List<Float> value) {
            this.dateTimes = dateTimes;
            this.values = value;
        }

        private List<String> dateTimes;
        private List<Float> values;

        public List<String> getDateTime() {
            return dateTimes;
        }

        public void setDateTime(List<String> dateTimes) {
            this.dateTimes = dateTimes;
        }

        public List<Float> getValues() {
            return values;
        }

        public void setValues(List<Float> values) {
            this.values = values;
        }
    }
}
