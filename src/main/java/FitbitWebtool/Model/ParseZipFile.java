package FitbitWebtool.Model;

//Import statements.
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.PosixFilePermissions;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * @author Tom Wagenaar
 * @version 0.0.4
 * Date: 11-12-2019
 */

public class ParseZipFile {
    // Instance variables declaration.

    /**
     * A method that receives a pathway to a zip file and a designated directory to where the files need to be stored,
     * unzips it and copies all the files to another directory. It creates a directory whenever the one that is
     * specified doesn't exists. At last all the connections to the files and entries are closed to counter data leaks.
     * @throws IOException, whenever a file is read with stream and it closes the connection (example).
     */
    public static void parseZip(String completeFilePath) throws IOException {
        String uploadFolderPath = completeFilePath.replace(".zip", "");

        // Use the pathway from the designated directory to check if that directory exists.
        File directoryFolder = new File(uploadFolderPath);

        // Make the newFile readable and writable.
        directoryFolder.setWritable(true);
        directoryFolder.setReadable(true);
        directoryFolder.setExecutable(true);

        // Create a directory whenever it doesn't exist.
        if (!directoryFolder.exists()) directoryFolder.mkdirs();

        // Make the newFile readable and writable.
        directoryFolder.setWritable(true);
        directoryFolder.setReadable(true);
        directoryFolder.setExecutable(true);

        // Make a variable that will obtain input bytes from a file.
        FileInputStream fileInpStream;

        // Buffer for read and write data to file
        byte[] buffer = new byte[1024];

        try {
            // Contains the input stream of the zip.
            fileInpStream = new FileInputStream(completeFilePath);
            ZipInputStream zipInpStream = new ZipInputStream(fileInpStream);

            // Get the first file in the zip.
            ZipEntry entriesZip = zipInpStream.getNextEntry();

            while (entriesZip != null) {
                // Get the filename from each file in the zip.
                String fileName = entriesZip.getName();

                // Remove the first part of the fileName that contains the FitBits username.
                fileName = fileName.substring(fileName.indexOf("/") + 1);

                // Make a new file in the designated directory.
                File newFile = new File(uploadFolderPath + File.separator + fileName);

                System.out.println("Unzipping to " + newFile.getAbsolutePath());

                // Create directories for sub directories in zip
                new File(newFile.getParent()).mkdirs();

                // Make an object that contains the output stream of the newly created file in the designated directory.
                FileOutputStream fileOutputStream = new FileOutputStream(newFile);

                // Make a variable that holds the length of the amount of files in the zip file.
                int len;
                while ((len = zipInpStream.read(buffer)) > 0) {
                    fileOutputStream.write(buffer, 0, len);
                }

                // Close the output files.
                fileOutputStream.close();
                //close this ZipEntry
                zipInpStream.closeEntry();
                // Get the next file in the zip folder.
                entriesZip = zipInpStream.getNextEntry();
            }
            //close last ZipEntry
            zipInpStream.closeEntry();
            zipInpStream.close();
            fileInpStream.close();

            // Delete the .zip file after unpacking, a new file is created because the File object has the delete
            // function.
            File file = new File(completeFilePath);
            file.delete();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}