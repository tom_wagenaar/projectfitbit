const options = {
    spanGaps: false,
    scales: {
        yAxes: [{
            ticks: {
                suggestedMin: 30,
                suggestedMax: 190,
                autoSkip: 10,
                autoSkipPadding: 10
            },
            scaleLabel: {
                display: true,
                labelString: 'bpm'
            }
        }],
        xAxes: [{
            ticks: {
                autoSkip: true,
                maxRotation: 30
            },
            scaleLabel: {
                display: true,
                labelString: 'time'
            }
        }],
    }
};

const stackedOptions = {
    options: {
        scales: {
            xAxes: [{
                stacked: true,
            }],
            yAxes: [{
                stacked: true,
            }]
        }
    }
};

function createSimpleChart(data, ylab, label, min, max, borderColor, type, elementId) {
    data.values = data.values.map(b => {
        if (b == 0)
            return null;
        return b;
    });
    console.log(type)
    if (type == "bar") {
        console.log("its a bar")
        chartData = {
            labels:data.dateTimes,
            datasets: [{
                data:data.values,
                label: ylab,
                backgroundColor: borderColor,
                borderColor: borderColor,
                fillColor:borderColor,
                fill: true
            }]
        };
    }

     else {
        chartData = {
            labels:data.dateTimes,
            datasets: [{
                data:data.values,
                label: ylab,
                borderColor: borderColor,
                fill: false
            }]
        };

    }
    var customOptions = $.extend( true, {}, options );
    customOptions.scales.yAxes[0] = [{
        ticks: {
            suggestedMin: min,
            suggestedMax: max,
            autoSkip: 10,
            autoSkipPadding: 10
        },
        scaleLabel: {
            display: true,
            labelString: ylab
        }
    }];

    new Chart(document.getElementById(elementId), {
        type: type,
        data: chartData,
        options: stackedOptions
    });
}

function createStackedBarChart(dataList,labels,colors, ylab, label, min, max, borderColor, type, elementId) {
    console.log("data list");
    console.log(dataList);
    let extractedData = [];
    console.log(dataList.length);

    for (i = 1; i < dataList.length; i++) {
        dataList[i].values = dataList[i].values.map(b => {
            if (b == 0)
                return null;
            return b;
        });


        //TODO make sure this actually appends something
        console.log(labels[i-1]);
        extractedData.push({
            backgroundColor: colors[i-1],
            label: labels[i-1],
            data: dataList[i].values
        });
    }
    console.log("extracted data");
    console.log(extractedData);
    var customOptions = $.extend( true, {}, stackedOptions );
    const chartData = {
        labels: dataList[0],
        datasets: extractedData
    };
    console.log("asfffffffffff");
    console.log({
        type: type,
        data: chartData,
        options: customOptions
});
    new Chart(document.getElementById(elementId), {
        type: type,
        data: chartData,
        options: customOptions
    });
}

fetch('/data')
    .then(function(response) {
        return response.json();
    })
    .then(function(myJson) {

        var dataHeartRate = myJson.dataHeartRate;
        var dataRHeartRate = myJson.dataRHeartRate;
        var dataCalories = myJson.dataCalories;
        var dataDistance = myJson.dataDistance;
        var dataAltitude = myJson.dataAltitude;
        var dataSteps = myJson.dataSteps;
        let dataActivity = [
            myJson.dataSedentaryMins.dateTimes,
            myJson.dataSedentaryMins,
            myJson.dataLightlyMins,
            myJson.dataModerateMins,
            myJson.dataVeryActiveMins];

        //dataHeartRate chart -----------
        createSimpleChart(dataHeartRate,
            "Complete heart rate",
            "Heart rate",
            30,190,
            'rgba(150, 0, 30, 1)',
            'line',
            'heartRateChart');



        //dataRHeartRate chart -----------
        createSimpleChart(dataRHeartRate,
            "Resting heart rate",
            "Resting heart rate",
            30,190,
            'rgba(150, 0, 30, 1)',
            'line',
            'restingHeartRateChart');


        //calories chart -----------
        createSimpleChart(dataCalories,
            "calories",
            "Calories burned",
            1,10,
            'rgba(180, 150, 10, 1)',
            'bar',
            'caloriesChart');


        //distance chart -----------
        createSimpleChart(dataDistance,
            "distance",
            "distance traveled",
            0,10,
            'rgba(0, 80, 100, 1)',
            'bar',
            'distanceChart');

        //altitude chart -----------
        createSimpleChart(dataAltitude,
            "altitude",
            "altitude gained",
            10,30,
            'rgba(0, 80, 100, 1)',
            'bar',
            'altitudeChart');

        //steps chart -----------
        createSimpleChart(dataSteps,
            "steps",
            "steps",
            0,120,
            'rgba(0, 80, 100, 1)',
            'bar',
            'stepsChart');

        //activity chart
        createStackedBarChart(dataActivity,["sedentary",'light','moderate','very active'],
            ['rgb(204, 153, 0)','rgb(0, 150, 200)','rgba(0, 200, 80, 1)','rgba(0, 100, 0, 1)'],
            'activity in minutes',
            'daily activity',
            0,10,
            'rgba(150, 0, 30, 1)',
            'bar',
            'activityChart')
    });
